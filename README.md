# Markdown JIRA Preview

[![](https://vsmarketplacebadge.apphb.com/version/transnano.markdown-jira-preview.svg)](https://marketplace.visualstudio.com/items?itemName=transnano.markdown-jira-preview)

Adds JIRA syntax support to VS Code's built-in Markdown preview

![example](https://gitlab.com/transnano/markdown-jira-preview/raw/master/docs/ex.png)

## Features

- Adds support for JIRA syntax support to VS Code's built-in Markdown preview
