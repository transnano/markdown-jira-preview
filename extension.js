"use strict";
var j2m = require("jira2md");

function activate(context) {
  return {
    extendMarkdownIt(md) {
      const highlight = md.options.highlight;
      md.options.highlight = (code, lang) => {
        if (lang && lang.match(/\bjira\b/i)) {
          console.log(j2m.jira_to_html(code));
          return `<div class="jira">${j2m.jira_to_html(code)}</div>`;
        }
        return highlight(code, lang);
      };
      return md;
    },
  };
}
exports.activate = activate;
